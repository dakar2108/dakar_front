<a name="2.0.3"></a>
## [2.0.3](https://github.com/advanced-rest-client/arc-icons/compare/2.0.1...2.0.3) (2018-05-17)




<a name="2.0.2"></a>
## [2.0.2](https://github.com/advanced-rest-client/arc-icons/compare/2.0.1...2.0.2) (2018-03-08)




<a name="2.0.1"></a>
## [2.0.1](https://github.com/advanced-rest-client/arc-icons/compare/1.0.31...2.0.1) (2018-03-08)


### New

* Adding tests ([eff4769aa2b4f51e45135b5a9395027c55e07e2e](https://github.com/advanced-rest-client/arc-icons/commit/eff4769aa2b4f51e45135b5a9395027c55e07e2e))
* Releaseing version 2 that supports Polymer 2.0 ([a5cb54adb2a6196ba222a8bd3aa705cf09908ee9](https://github.com/advanced-rest-client/arc-icons/commit/a5cb54adb2a6196ba222a8bd3aa705cf09908ee9))

### Update

* Updated Travis configuration to connect to Sauce Labs. ([ac542a26afd1b2a634f5d8bacb65a6df59402282](https://github.com/advanced-rest-client/arc-icons/commit/ac542a26afd1b2a634f5d8bacb65a6df59402282))



<a name="1.0.31"></a>
## [1.0.31](https://github.com/advanced-rest-client/arc-icons/compare/1.0.30...1.0.31) (2018-01-10)




<a name="1.0.30"></a>
## [1.0.30](https://github.com/advanced-rest-client/arc-icons/compare/1.0.29...1.0.30) (2018-01-10)


### New

* Adding star icons. ([7d92bab3813302186039ab154f345eb124009743](https://github.com/advanced-rest-client/arc-icons/commit/7d92bab3813302186039ab154f345eb124009743))



<a name="1.0.29"></a>
## [1.0.29](https://github.com/advanced-rest-client/arc-icons/compare/1.0.28...1.0.29) (2017-12-22)




<a name="1.0.28"></a>
## [1.0.28](https://github.com/advanced-rest-client/arc-icons/compare/1.0.27...1.0.28) (2017-12-22)


### New

* Adding comment icon ([c6b474e2a5f2793f07f9883bcede60d9f61f5a07](https://github.com/advanced-rest-client/arc-icons/commit/c6b474e2a5f2793f07f9883bcede60d9f61f5a07))



<a name="1.0.27"></a>
## [1.0.27](https://github.com/advanced-rest-client/arc-icons/compare/1.0.26...1.0.27) (2017-11-21)




<a name="1.0.26"></a>
## [1.0.26](https://github.com/advanced-rest-client/arc-icons/compare/1.0.25...1.0.26) (2017-11-21)


### New

* Added RAML logo. ([ff3f00e7703cd9e8845ebe6a49f705d73442a6cf](https://github.com/advanced-rest-client/arc-icons/commit/ff3f00e7703cd9e8845ebe6a49f705d73442a6cf))



<a name="1.0.25"></a>
## [1.0.25](https://github.com/advanced-rest-client/arc-icons/compare/1.0.24...1.0.25) (2017-11-20)




<a name="1.0.24"></a>
## [1.0.24](https://github.com/advanced-rest-client/arc-icons/compare/1.0.23...1.0.24) (2017-11-20)


### New

* Added list view icon. ([1c2448e12803baaeeac5989fc774b411743eb4e4](https://github.com/advanced-rest-client/arc-icons/commit/1c2448e12803baaeeac5989fc774b411743eb4e4))

### Update

* Removed anypoint icon definition. ([5a7003c198aac6f9fbf60c61b3d867a23cf1e2a7](https://github.com/advanced-rest-client/arc-icons/commit/5a7003c198aac6f9fbf60c61b3d867a23cf1e2a7))



<a name="1.0.23"></a>
## [1.0.23](https://github.com/advanced-rest-client/arc-icons/compare/1.0.21...1.0.23) (2017-09-21)




<a name="1.0.22"></a>
## [1.0.22](https://github.com/advanced-rest-client/arc-icons/compare/1.0.21...v1.0.22) (2017-06-20)




<a name="1.0.21"></a>
## [1.0.21](https://github.com/advanced-rest-client/arc-icons/compare/1.0.20...v1.0.21) (2017-06-20)




<a name="1.0.20"></a>
## [1.0.20](https://github.com/advanced-rest-client/arc-icons/compare/1.0.19...v1.0.20) (2017-06-18)




<a name="1.0.19"></a>
## [1.0.19](https://github.com/advanced-rest-client/arc-icons/compare/1.0.18...v1.0.19) (2017-06-18)


### Update

* reverted lint command ([5c01d771665200a69115e3cb60d8f3b1a53eb2a0](https://github.com/advanced-rest-client/arc-icons/commit/5c01d771665200a69115e3cb60d8f3b1a53eb2a0))
* Updated lint command ([3c975c13afd21f1c5dcb20e010841c90f88cf602](https://github.com/advanced-rest-client/arc-icons/commit/3c975c13afd21f1c5dcb20e010841c90f88cf602))
* Updated travis configuration ([859b2d1a21dd7fb243bd224e8d222afe1f86e749](https://github.com/advanced-rest-client/arc-icons/commit/859b2d1a21dd7fb243bd224e8d222afe1f86e749))



<a name="1.0.18"></a>
## [1.0.18](https://github.com/advanced-rest-client/arc-icons/compare/1.0.17...v1.0.18) (2017-04-04)




<a name="1.0.17"></a>
## [1.0.17](https://github.com/advanced-rest-client/arc-icons/compare/1.0.15...v1.0.17) (2017-04-04)




<a name="1.0.16"></a>
## [1.0.16](https://github.com/advanced-rest-client/arc-icons/compare/1.0.15...v1.0.16) (2017-04-04)




<a name="1.0.15"></a>
## [1.0.15](https://github.com/advanced-rest-client/arc-icons/compare/1.0.14...v1.0.15) (2017-03-31)


### Update

* Added home icon and anypoint pin icon ([c9400239b840f30fa78ed92d3675bd198f2460a2](https://github.com/advanced-rest-client/arc-icons/commit/c9400239b840f30fa78ed92d3675bd198f2460a2))
* Removed sauce connect option from travis configuration ([29abd97676eb3d99b4e6966441db6d1ab4b22023](https://github.com/advanced-rest-client/arc-icons/commit/29abd97676eb3d99b4e6966441db6d1ab4b22023))



<a name="1.0.14"></a>
## [1.0.14](https://github.com/advanced-rest-client/arc-icons/compare/1.0.12...v1.0.14) (2017-01-28)


### Update

* Added missing icons from ARC ([c6ee20de76806b5a844608d8a7480247cf9d4225](https://github.com/advanced-rest-client/arc-icons/commit/c6ee20de76806b5a844608d8a7480247cf9d4225))



<a name="1.0.13"></a>
## [1.0.13](https://github.com/advanced-rest-client/arc-icons/compare/1.0.12...v1.0.13) (2017-01-28)




<a name="1.0.12"></a>
## [1.0.12](https://github.com/advanced-rest-client/arc-icons/compare/1.0.8...v1.0.12) (2017-01-04)


### Update

* Added error icon ([48ab672f0caeb7dcc0b40f9f43beca393f22eef8](https://github.com/advanced-rest-client/arc-icons/commit/48ab672f0caeb7dcc0b40f9f43beca393f22eef8))
* Added menu icon ([f01e021d1f6333b317d67a9552b5e6fa54afd3bf](https://github.com/advanced-rest-client/arc-icons/commit/f01e021d1f6333b317d67a9552b5e6fa54afd3bf))
* Added new Travis configuration ([32195619993e54b064f6be8680aac772c7fe9b2a](https://github.com/advanced-rest-client/arc-icons/commit/32195619993e54b064f6be8680aac772c7fe9b2a))
* Added new Travis configuration ([99df40e13eec1a32339fd2935774fa051d1f3e02](https://github.com/advanced-rest-client/arc-icons/commit/99df40e13eec1a32339fd2935774fa051d1f3e02))



<a name="1.0.11"></a>
## [1.0.11](https://github.com/advanced-rest-client/arc-icons/compare/1.0.8...v1.0.11) (2016-12-21)


### Update

* Added menu icon ([f01e021d1f6333b317d67a9552b5e6fa54afd3bf](https://github.com/advanced-rest-client/arc-icons/commit/f01e021d1f6333b317d67a9552b5e6fa54afd3bf))
* Added new Travis configuration ([32195619993e54b064f6be8680aac772c7fe9b2a](https://github.com/advanced-rest-client/arc-icons/commit/32195619993e54b064f6be8680aac772c7fe9b2a))
* Added new Travis configuration ([99df40e13eec1a32339fd2935774fa051d1f3e02](https://github.com/advanced-rest-client/arc-icons/commit/99df40e13eec1a32339fd2935774fa051d1f3e02))



<a name="1.0.10"></a>
## [1.0.10](https://github.com/advanced-rest-client/arc-icons/compare/1.0.8...v1.0.10) (2016-12-21)


### Update

* Added menu icon ([f01e021d1f6333b317d67a9552b5e6fa54afd3bf](https://github.com/advanced-rest-client/arc-icons/commit/f01e021d1f6333b317d67a9552b5e6fa54afd3bf))
* Added new Travis configuration ([32195619993e54b064f6be8680aac772c7fe9b2a](https://github.com/advanced-rest-client/arc-icons/commit/32195619993e54b064f6be8680aac772c7fe9b2a))
* Added new Travis configuration ([99df40e13eec1a32339fd2935774fa051d1f3e02](https://github.com/advanced-rest-client/arc-icons/commit/99df40e13eec1a32339fd2935774fa051d1f3e02))



<a name="1.0.9"></a>
## [1.0.9](https://github.com/advanced-rest-client/arc-icons/compare/1.0.8...v1.0.9) (2016-11-20)


### Update

* Added new Travis configuration ([32195619993e54b064f6be8680aac772c7fe9b2a](https://github.com/advanced-rest-client/arc-icons/commit/32195619993e54b064f6be8680aac772c7fe9b2a))
* Added new Travis configuration ([99df40e13eec1a32339fd2935774fa051d1f3e02](https://github.com/advanced-rest-client/arc-icons/commit/99df40e13eec1a32339fd2935774fa051d1f3e02))



