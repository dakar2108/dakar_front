<a name="2.0.2"></a>
## [2.0.2](https://github.com/advanced-rest-client/paper-combobox/compare/1.0.0...2.0.2) (2018-03-13)




<a name="2.0.1"></a>
## [2.0.1](https://github.com/advanced-rest-client/paper-combobox/compare/1.0.0...2.0.1) (2018-03-13)




<a name="0.1.3"></a>
## [0.1.3](https://github.com/advanced-rest-client/paper-combobox/compare/0.1.2...0.1.3) (2018-02-09)




<a name="0.1.2"></a>
## 0.1.2 (2018-02-09)


### Fix

* Added description for the  property for the linter ([4418dac8921ecddba9d98cbe9c461aeb41fb14a8](https://github.com/advanced-rest-client/paper-combobox/commit/4418dac8921ecddba9d98cbe9c461aeb41fb14a8))
* Fixing bower command ([4a65e6a200b53916e26ef3e131c88a458f03a994](https://github.com/advanced-rest-client/paper-combobox/commit/4a65e6a200b53916e26ef3e131c88a458f03a994))

### Update

* Added sauce configuration for tests ([4e681411b1f78f198eeeb6033dc67bccdcfb1aa7](https://github.com/advanced-rest-client/paper-combobox/commit/4e681411b1f78f198eeeb6033dc67bccdcfb1aa7))
* removed legacy browsers from tests ([24322a3c604520934c5c929240f4d9133f3b3587](https://github.com/advanced-rest-client/paper-combobox/commit/24322a3c604520934c5c929240f4d9133f3b3587))
* Updated styling definitions for the element. ([f77363b893c1ab1ce8d2e3ad2f663d139ddd3a30](https://github.com/advanced-rest-client/paper-combobox/commit/f77363b893c1ab1ce8d2e3ad2f663d139ddd3a30))



<a name="0.1.1"></a>
## 0.1.1 (2017-09-01)


### Fix

* Added description for the  property for the linter ([4418dac8921ecddba9d98cbe9c461aeb41fb14a8](https://github.com/advanced-rest-client/paper-combobox/commit/4418dac8921ecddba9d98cbe9c461aeb41fb14a8))
* Fixing bower command ([4a65e6a200b53916e26ef3e131c88a458f03a994](https://github.com/advanced-rest-client/paper-combobox/commit/4a65e6a200b53916e26ef3e131c88a458f03a994))

### Update

* Added sauce configuration for tests ([4e681411b1f78f198eeeb6033dc67bccdcfb1aa7](https://github.com/advanced-rest-client/paper-combobox/commit/4e681411b1f78f198eeeb6033dc67bccdcfb1aa7))
* removed legacy browsers from tests ([24322a3c604520934c5c929240f4d9133f3b3587](https://github.com/advanced-rest-client/paper-combobox/commit/24322a3c604520934c5c929240f4d9133f3b3587))



